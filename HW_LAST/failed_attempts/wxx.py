import numpy as np
import cv2
import mss
import time
import pyautogui
import wx


# Part of the screen to capture
#monitor = {"top": 0, "left": 0, "width": 1440, "height": 900}
#monitor = {"top": 0, "left": 0, "width": 1440, "height": 900}

app = wx.App()

    
while True:
    last_time = time.time()

    screen = wx.ScreenDC()
    size = screen.GetSize()
    bmp = wx.Bitmap(size[0], size[1])
    mem = wx.MemoryDC(bmp)
    mem.Blit(0, 0, size[0], size[1], screen, 0, 0)
    del mem  # Release bitmap
    #bmp.SaveFile('screenshot.png', wx.BITMAP_TYPE_PNG)

#    cv2.imshow('YOLO', img)

    print("fps: {}".format(1 / (time.time() - last_time)))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break

print('Done.')
