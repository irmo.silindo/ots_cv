import numpy as np
import cv2
import mss
import time
import pyautogui


# Part of the screen to capture
#monitor = {"top": 0, "left": 0, "width": 1440, "height": 900}
monitor = {"top": 0, "left": 0, "width": 1440, "height": 900}
sct = mss.mss()
    
while True:
    last_time = time.time()

    img = np.array(sct.grab(monitor))
 
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    cv2.imshow('YOLO', img)

    print("fps: {}".format(1 / (time.time() - last_time)))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        print(img.shape)
        cv2.destroyAllWindows()
        break

print('Done.')
