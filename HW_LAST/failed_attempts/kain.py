import cv2
import numpy as np
import os
from time import time
import pyautogui
from ultralytics import YOLO
from mss import mss
#from windowcapture import WindowCapture

# Change the working directory to the folder this script is in.
# Doing this because I'll be putting the files from each video in their own folder on GitHub
os.chdir(os.path.dirname(os.path.abspath(__file__)))


# initialize the WindowCapture class

loop_time = time()
model = YOLO("yolov8n.pt")

while(True):

    # get an updated image of the game
    screen = pyautogui.screenshot()
    # Convert the output to a numpy array
    screen_array = np.array(screen)
    # Crop out the region we want - height, width, channels   
    cropped_region = screen_array[25:625, 1122:, :]
    # Convert the color channel order
    corrected_colors = cv2.cvtColor(cropped_region, cv2.COLOR_RGB2BGR)

    # Make detections 
    results = model(corrected_colors)
    
    #cv2.imshow('YOLO', np.squeeze(results.render()))

    #cv2.imshow('YOLO', np.squeeze(results[0].plot()[..., ::-1]))
    cv2.imshow('YOLO', results[0].plot()[..., ::-1])
    #cv2.imshow('YOLO', corrected_colors[..., ::-1])

    ####cv.imshow('Computer Vision', screenshot)

    # debug the loop rate
    print('FPS {}'.format(1 / (time() - loop_time)))
    loop_time = time()

    # press 'q' with the output window focused to exit.
    # waits 1 ms every loop to process key presses
    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break

print('Done.')
