# ots_cv Last_HW


## Testing Environmet
- MacBook Pro 2018 (Intel CPU + GPU)
- Python 3.11.6
- Android Studio Hedgehog | 2023.1.1 RC 3 (We need it for Android Emulator (x86-64))
- Arcadium - Space War (1.24) ( https://play.google.com/store/apps/details?id=com.ihgyug.arcadium2&hl=en&gl=US&pli=1 ) (apk downloaded via apkcombo.com)

## Python Modules
- pip install ultralytics
- pip install vidgear
- pip install pyscreenshot
- pip install pywinctl
- pip install pyautogui
- pip install mss
- pip install onnx
- pip install openvino


## Tools Used
- https://www.makesense.ai to create annotations
- https://abhitronix.github.io/vidgear for fast scree capture
- Ultralytics YOLOv8 as initial detection model
- OpenVino as detection speed up
- pyautogui to make clicks
- Google


## Getting started

- Don't forget to replace `window_name` variable to your's (before running .py scripts)


## Project Structure

- ./gear_collect.py - script to capture raw images from android emulator window (images writed to ./data/images folder) (android emulator should be started manually before running this script)
- data/images - core images collected with script ./gear_collect.py
- data/images_labeled - images with added annotations in YOLO format from makesense.ai
- datasets/arcadiumdataset - formed dataset in YOLO format with images and annotations from ./data/images_labeled
- ./arcadiumdataset_v8.yaml - config file for arcadiumdataset
- Last_HW.ipynb - notebook used to train model
- model - folder contaiing traied models
- ./gear_bot.py - small script to capture and manipulate game (you should install and start apk manually before runing this script)

