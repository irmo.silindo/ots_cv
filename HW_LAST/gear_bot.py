import numpy as np
import cv2
import time
from vidgear.gears import ScreenGear
from ultralytics import YOLO
import pyautogui
import uuid
import os
import time
import pygetwindow as gw
import random

os.chdir(os.path.dirname(os.path.abspath(__file__)))

############## Window Name ###################
window_name = "qemu-system-x86_64 Android Emulator - Pixel_7_API_34:5554"


############# Model ####################
model = YOLO("model/best_100.pt")
#model = YOLO("model/best_openvino_model/") # Быстрее, но в ней по-другому хранятся классы ()


############# Get Windows & Streamer ####################

titles = gw.getAllTitles()
print(titles)

windows = []
for win in titles:
    geometry = gw.getWindowGeometry(win)
    if win == window_name:
        print(f'Window title: {win}')
        print(f'> top-left X coordinate: {geometry[0]}')
        print(f'> top-left Y coordinate: {geometry[1]}')
        print(f'> width: {geometry[2]}')
        print(f'> height: {geometry[3]}\n')
        windows.append(geometry)

w = windows[0]

w_x = w[0]
w_y = w[1]
w_width = w[2]
w_height = w[3]

aspect_x = None
aspect_y = None


print('pyAutoGuiSize=', pyautogui.size())
print(w_x, w_y, w_width, w_height)

#colorspace="COLOR_RGBA2BGR"
#backend="mss" / "PIL"
#options = {'top': 25, 'left': 0, 'width': 300, 'height': 600}
options = {'top': int(w_x), 'left': int(w_y), 'width': int(w_x + w_width), 'height': int(w_y + w_height)}
print(options)
stream = ScreenGear(logging=False, backend="mss", **options).start()


############# Bot Game #####################
#represent our classes:

pause = None
player = None
start = None
choose = None
choose_ok = None
enemies = []

touch_pad = None # point to control ship
press_start_time = None

def get_box_center(box):
    (left, top, right, bottom) = box
    return ( int((left + ((right - left)/2)).item()), int((top + ((bottom - top)/2)).item()) )

def game(model, results):
    global start
    global player
    global pause
    global choose
    global choose_ok
    global enemies

    global touch_pad

    boxes = results.boxes

    for box in boxes:
        b = box.xyxy[0] # get box coordinates in (left, top, right, bottom) format
        c = model.names[int(box.cls)]


        if c == 'start':
            start = get_box_center(b)
            print("start center= ", start[0], start[1])


        if c == 'player':
            player = get_box_center(b)
            print("player center= ", player[0], player[1])


        if c == 'pause':
            pause = get_box_center(b)
            print("pause center= ", pause[0], pause[1])
            (x, y) = pause
            touch_pad = (int(w_width/2), y)


        if c == 'choose':
            choose = get_box_center(b)

        
        if c == 'choose_ok':
            choose_ok = get_box_center(b)
            print("choose_ok center= ", choose_ok[0], choose_ok[1])


        if c.startswith('enemy_'):
            enemies.append(get_box_center(b))


def clearState():
    global pause
    global player
    global start
    global choose
    global choose_ok
    global enemies

    pause = None
    player = None
    start = None
    choose = None
    choose_ok = None
    enemies = []


def clickStart():
    global start
    global press_start_time

    press_start_time = None

    start_x = int(start[0] * aspect_x)
    start_y = int(start[1] * aspect_y)
    print('click start at', start_x, start_y)
    pyautogui.moveTo(x = w_x + start_x , y = w_y + start_y)
    pyautogui.drag(10, 0, 0.25, button='left')



def clickChooseOk():
    global choose_ok

    ch_x = int(choose_ok[0]) * aspect_x
    ch_y = int(choose_ok[1]) * aspect_y
    print('click choose_ok at', ch_x, ch_y)
    pyautogui.moveTo(x = w_x + ch_x , y = w_y + ch_y)
    pyautogui.drag(10, 0, 0.25, button='left')


def decideMove():
    print('decide move func')


    #Trully decide
    if len(enemies) > 0:
        mean_x = 0
        mean_y = 0

        for enemy in enemies:
            mean_x = mean_x + enemy[0]
            mean_y = mean_y + enemy[1]

        mean_x = int(mean_x / len(enemies))
        mean_y = int(mean_y / len(enemies))

        dx = mean_x - player[0]
        dy = mean_y - player[1]
        if dx == 0:
            dx = 1
        if dy == 0:
            dy = 1
        dx = -int(dx / dx) * 10
        dy = -int(dy / dy) * 10

    if len(enemies) == 0:
        #Random moves
        moves = [0, 10, -10]
        dx = random.choice(moves)
        dy = random.choice(moves)


    return dx, dy


def moveShip():
    global touch_pad
    global press_start_time

    tp_x = int(touch_pad[0]) * aspect_x
    tp_y = int(touch_pad[1]) * aspect_y
    print('click touch_pad at', tp_x, tp_y)

    dx, dy = decideMove()
    print('Pressing touchpad')
    pyautogui.moveTo(x = w_x + tp_x, y = w_y + tp_y)
    pyautogui.drag(dx, dy, 0.2, button='left')
    press_start_time = time.time() #Запоминаем время нажаия


def bot():
    global start
    global choose_ok
    global pause

    print('entering bot() function')
    #print(start)
    if start != None:
        clickStart()
        clearState()

    if choose_ok != None:
        clickChooseOk()

    if (pause != None) & (player != None):
        moveShip()
    
    

#################### Main Loop #####################
while True:
    last_time = time.time()

    frame = stream.read()

    if frame is None:
        break

    
    #For mss backend
    drop_alpha = frame[:,:,:3] # BGR
    rgb = cv2.cvtColor(drop_alpha, cv2.COLOR_BGR2RGB)

    #For PIL backend
    # drop_alpha = frame[..., ::-1][:,:,:3][..., ::-1]
    # rgb = cv2.cvtColor(drop_alpha, cv2.COLOR_BGR2RGB)

    results = model(drop_alpha)
    plot = results[0].plot()

    plot_height = plot.shape[0]
    plot_widht = plot.shape[1]

    print('plot_size=', plot_widht, plot_height)
    aspect_x = w_width / plot_widht
    aspect_y = w_height / plot_height

    cv2.imshow('YOLO', plot)

    if len(results) > 0:
        print('process')
        game(model = model, results = results[0])
        bot()
        clearState()

    # print("fps: {}".format(1 / (time.time() - last_time)))
    

    if cv2.waitKey(1) & 0xFF == ord('q'):
        print(drop_alpha[1])
        print(drop_alpha.shape)
        cv2.destroyAllWindows()
        stream.stop()
        break

print('Done.')
cv2.destroyAllWindows()
stream.stop()

